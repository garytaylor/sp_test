# SP Test

This project demonstrates the use of a ruby gem to parse a log file and produce
a report showing the number of visits per page, with the most visited pages at the top
of the report.

## Approach

Trying to think of user requirements first, if I were to use a ruby script
to parse a log file, I would not want to have to clone a repository, run
a bundle install etc.. before I was able to run it.

So, my first key decision was to write this as a ruby gem, with all development / test
dependencies in the Gemfile and all runtime dependencies in the gemspec file.

If this were more real life than a test, I would check to see if this was a requirement
as it means the users simply install the gem and then do not care about where it
is installed - so they can just use it on the command line from anywhere and
the executable file will be in the path.

But, this is a test scenario, so think of it as just showing off a little really  that I care
about the users using my script and I want their lives to be as easy as possible, just
like mine is when i just use 'rspec' or 'rails'  etc.. and they just work.  

I believe I have considered the overhead of doing this as small also.  If the overhead 
was significant, this decision would probably have been left for another day, so we
can get out minimum viable product out to market earlier.

My approach would then be to go through a high level design (see next section) to give me an idea where
to start - then write my first integration test that works on the script itself,
it will use ideally no mocks - the idea being that this is my 'End Goal' - this is
the main thing that needs to pass - and when it does, I will celebrate knowing that
my 'positive flow' seems to be working.

Much more detailed tests will then follow as I am guided by the errors given by this high
level integration test.  In general, I will not write code until a test failure
is telling me to.

I will then keep going with lower level tests, moving my focus in on individual parts
of the utility, focussing to start with on the positive flow until everything works.

Then, I will move on to the negative flows and edge cases.  The integration test will
prove that something going wrong is presented to the user in a standard way using a standard
interface from the underlying code, but then the details of what errors are caught
and presented are done in the lower level tests.  This is mainly for speed of execution (although
not so much so in a command line utility), but also for separation of concerns.  It is very important
that the tests are easy to read and understand, so if I come back to it in 6 months time,
or a colleague needs to make a change, it is very clear from the tests what the whole
thing is supposed to do.

## Design

Whilst a command line utility can be tested using rspec, I much prefer the 
inner workings of the utility to be done using traditional modules and classes,
the idea being that the 'command line' part of this utility is simply a 'user interface'
and the workings can be tested outside of this environment with a final integration test
proving that it works 'end to end'.  

In general, when I design software, I think about what would happen if the customer
asked for it to be used in a different environment - such as a web browser, a slack bot or 
some other weird thing that I haven't thought of.  If the list of changes to switch to another
'front end' for the code doing the work, then I have probably got something wrong.

I could also apply this the other way around (and probably more likely), if the customer
switched web servers and wanted to parse a different log file, again, if the list of changes to
the command line script would be large to switch, then I have probably got something
wrong.

Also, whilst not specified, this is the sort of tool that could gain some
command line switches in the future - e.g. different output formats, different input formats
etc..  Whilst I would not write any of this functionality without it being a requirement now,
I would consider the basic infrastructure to be able to easily add it at a later stage.

So, maybe the command line script creates an instance of a Cli class (Command Line Interpreter) to start
with, which is passed the arguments from the environment

Which, then gets me on to thinking about using the fabulous 'thor' gem for command
line parsing.  Whilst my concern is that it might make the reviewer think that I
don't know how to parse command line parameters using ARGV, I would typically use thor for this
task as it means writing help strings for the utility is made very easy and I can just
get on with what I would describe as the 'Important business logic'.

Once past the command line parsing, the rest of the code apart from the 'reporter' which I will
mention last, will not care about what called it - it could be a command line parser,
it could be a web server process, a slack bot etc..

I ended up not using thor as it was not really intended for a simple use case like ours, but more
for when the utility needs multiple commands.

The general design then flows like this :-

The 'Cli' class is the user interface which is responsible for validating what it can and then
passing control over to the 'ReportBuilder'.  The ReportBuilder is a utility class to allow the
chaining of methods to progressively specify what is required - such as type of report, input format,
output type etc.. etc..

The ReportBuilder will build 4 things - from 'input to output', these are

* The Input adapter
* The Parser
* The Report
* The Output adapter

Each one of these has its own 'factory class' - called 'Input', 'Parser', 'Report' and 'Output'.
The factory class is just to prevent the caller from knowing about how to convert from a 
symbol representing the item (i.e. a :file, a :text version of the :page_hits report etc..) to the 
actual instance of the correct class.

Then, the ReportBuilder will chain all of these items together using their 'each' command - as each
one is an enumerator, the first one being lazy, which means that the rest in the chain will also
be lazy.

The use of lazy enumerators allowed the writing of various 'processor methods' so that they do 1 thing
only.  Without lazy enumerators, this would have produced many iteration loops instead of just 1.

So, to start the process going, the ReportBuilder calls the 'call' method of the output adapter which
is responsible for taking its input enumerator and writing the content to the output (in this case, stdout).

So, the output adapters input enumerator is the return value of 'each' on the report, whose input enumerator
is the return value of 'each' from the parser, who's input enumerator is the return value of the input
adapter who's input source is the file.

So, the output adapter is effectively 'pulling' the data it needs from further up the chain.

## Installation

Install the gem as follows (after changing to this directory):

    $ rake install:local

## Running

Once installed, simply run like this (again, from this directory)

```
 log_reporter spec/fixtures/webserver.log
```

and you will see the output like this

```
|-------------------------------------------------------------------|----------|
| Path                                                              |  Unique  |
|                                                                   |  views   |
|-------------------------------------------------------------------|----------|
| /help_page/1                                                      |    23    |
| /contact                                                          |    23    |
| /home                                                             |    23    |
| /index                                                            |    23    |
| /about/2                                                          |    22    |
| /about                                                            |    21    |
|-------------------------------------------------------------------|----------|
```

## History

To enable understanding of how I work, I will add to the history as I progress,
so it is clear what order I do things etc..

1. Add spec/fixtures as our first fixture file
2. Make readme interesting
3. Added .gitignore as I use rubymine and I did not want all of its files in the repository
4. Used `bundle gem` command to produce skeleton for gem and updated readme to include gem installation
5. Filled in gemspec file to allow initial bundle install to be run - to produce lock file etc..
6. Setup groups in gemfile (whilst not technically required, as the Gemfile is only used in development anyway, but demonstrating what I would do if I were not developing a gem)
7. Installed rubocop and used auto correct to sort out all the warnings in skeleton code 
   so I am starting from zero warnings (stuck with default rubocop settings as they 
   are generally so subjective !  In real life, a project team might have existing preferences)
8. Added rubocop-rspec gem and installed into project (again, normally a team decision, but I like it)
9. Added first feature spec to expect the basics for now.  A negative flow will be added later (this spec is obviously failing)
10. Changed direction - no longer using thor as it is adding complexity that
    is not required and will not allow a default command with arguments as is
    a requirement in our case.  I am keeping the same interface as thor now though
    so if it is required in the future, the Cli class can extend thor.
    Added own Cli class with just some dummy text to prove that it fails in the
    right way for now.  On to testing the Cli on its own from that point.
11. After a short dispute with myself, I have moved towards using streams / iterators
    which the caller is responsible for passing on to the reporter.
    This should give a nice interface where the parser will yield a
    'normalized object' (to be defined) when requested by the reporter calling
    its 'each' method.  This will mean all reporters use this and all parser
    produce this.
12. Moved away from passing an IO or File object into the parser in favour
    of just using the filename.  This makes life simpler for the caller,
    but more importantly, allows the parser to handle errors such as 
    the file not found, file cannot be read etc..  It is expected that
    when the reporter calls the enumerator's each method it can yield either
    a normal data object or an error object.  This allows the reporter
    to decide what to do with errors and how they are presented to the user.
13. As basic unit tests were passing, I was expecting feedback from my feature
    spec, but it was not working properly as I was only checking for stdout
    which would not show any errors.  I have now added another spec to check 
    for errors to give better feedback during development
14. Enhanced the Cli spec as I had missed out the validation of creating the reporter
15. Rubocop tweaked to allow blocks of any size in the spec folder or any descendant    
16. Thrashed out the details of the parser_spec.rb file, trying to think of all edge cases etc..
17. Decided on a nicer interface for the Cli calling the main code - a 'Report Builder' object which is
    responsible for pulling together the 'input object' (responsible for getting the input stream),
    the 'parser object' (responsible for converting the text stream into internal objects),
    the 'report object' (responsible for converting the stream of objects into a text stream) 
    and the 'output object' (responsible for writing the text stream to its final destination)
    Once the builder has been fully built, the Cli calls 'call' on it and the
    work will get done
18. Added the spec for the ReportBuilder - not yet implemented anything but
    an empty class.  Tests giving wrong number of arguments as expected as the
    dependencies are injected by the tests and the constructor does not
    accept them yet.
19. Added the spec for the Output class - this is simply a factory class 
    responsible for producing instances of the correct type of output
    instance.  Our project only needs one at the moment, but this is allowing
    for easy extension without modifying existing code hopefully.    
    Again, no implementation yet - just writing the first iteration of 
    tests at this point.
20. Added the spec for the Output::StdOut class - this is responsible for
    taking the stream and presenting it to STDOUT without any transformation
21. Added the spec for the Report class - this is simply a factory class 
    responsible for producing instances of the correct type of report instance.
    Our project only needs 1 at the moment, but I felt it was important for
    this to be easily extended in the future as the client will no doubt
    ask for a new report to be added.
22. Added spec for Report::Text::PageHits with empty implementation
23. Added specs for Parser class and Parser::Webserver class. Following the 
    patterns above, the Parser class is the factory class and Parser::Webserver is
    the actual parser   
24. Added the input spec and the Input::File spec - again, the first is a factory class
    and the latter is responsible for producing an iterator to iterate over
    each line of data.
25. All specs done now - probably nowhere near 100%, but a good starting point.
    So, moving on to the implementation next.     
26. Tackled all the factory classes first, they are all very similar, may well even warrant
    having a superclass or a mixin module, but leaving this for now.
27. Added implementation for ReportBuilder and changed the way it works
    to store the arguments until a build method is called.  This build method
    may be called internally shortly and made private.
28. Re worked the CLI class to use the new report builder
29. Refactored the build method of the builder to be private - no need for
    caller to call it really.
30. Methodically progressed through all failures, focussing on the failures
    causing failures in the top level test to allow me to see progress towards
    the end goal.  Now 100% passing
31. Now time to think about more error handling and aesthetics
32. Whilst considering sorting, I decided to move to using the ip address
    to identify unique views, which I had not considered until now.               
    This might be something I would question normally as ip address is
    not the ideal information to use for this, as many users may report
    the same ip address if behind a proxy server for example.  But, this
    test does state unique views, so here goes.
    Because the 'LogReporter::Report::Text::PageHits' class is responsible
    for taking the individual rows and sorting / summing them, this is the 
    only code I will need to change - so into its test and write something
    to prove this works first of all (even though I know it won't - as I mentioned
    earlier, I rarely write code unless a failing test is telling me to)
33. Whilst I did not have the time to prove this, i assumed that grouping
    the data by path will mean not have to store every ip
    address against every path and store in one big hash just to ensure
    uniqueness.    
    To make the code a little neater, each transformation of the
    data is in its own method, but as we are using lazy enumerators,
    these will all be chained together so we only iterate the data once
34. All tests now passing and ran a quick test when i install the gem using 
    ```rake install:local``` (as I am not publishing to rubygems)
    The results were fine after adding the load path to the exe file
    However, error handling is not great at the moment, and whilst I won't
    go mad trying to make this super robust, I think the basics of a missing
    file should be handled, which it isnt at the moment.
35. Added example to cli spec - to prove error handling of missing file
    Implemented change in CLI and manually tested as well
36. Next, I am not particularly proud of the final output, I think if I were
    using this, I would expect it to be tabular.  I will quickly work on this now.
    As my tests dont look for exact text in the output, I will not be writing
    tests to test for presentational things like this - the tests will remain
    functional only, else we will be here forever.   
37. I was slightly wrong - I had checked for specific test in feature test,
    and I had to allow for header and footer rows in the report spec.  Now all
    passing and the output looks much better.
38. Added simplecov and checked coverage which is 97%         
    
## Next Steps

I would consider this the minimum viable product and there are things that I would develop further,
which are :-

1. Change the objects in the chain to be actual enumerators rather than calling to_enum which feels
   like it cheats a little as it creates a new object which is the enumerator which calls back to 
   my code.
2. Add examples to documentation in key classes
3. Develop error handling further - I currently have the concept of an 'invalid row', but never
   progressed to showing this to the user.  I think my next question to myself would be 'Is this
   really required ?' - if it was, I might enhance the interfaces of the objects to communicate errors
   separately from the data.
   
## Finally

Thank you for reading all of this, I spent a long time in this readme to communicate how I got from
zero code to a working solution and explaining my thinking along the way.   