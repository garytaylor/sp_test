# frozen_string_literal: true

RSpec.describe 'log_reporter integration', type: :feature do
  it 'appears to output the correct first line when given the example fixture' do
    expect { system('./exe/log_reporter spec/fixtures/webserver.log') }
      .to output(a_string_matching(%r{/help_page/1.*23})).to_stdout_from_any_process
  end

  it 'does not output anything to stderr' do
    expect { system('./exe/log_reporter spec/fixtures/webserver.log') }
      .not_to output.to_stderr_from_any_process
  end
end
