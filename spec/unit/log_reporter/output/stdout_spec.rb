# frozen_string_literal: true

require 'log_reporter/output/stdout'
RSpec.describe LogReporter::Output::Stdout do
  subject(:output_adapter) { described_class.new(source: fake_iterator) }

  let(:fake_iterator) { ['line 1', 'line 2'].each }

  describe '#call' do
    it 'outputs each line to stdout' do
      expect { output_adapter.call }.to output("line 1\nline 2\n").to_stdout
    end
  end
end
