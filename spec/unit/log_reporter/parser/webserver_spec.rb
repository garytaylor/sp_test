# frozen_string_literal: true

require 'log_reporter/parser/webserver'
RSpec.describe LogReporter::Parser::Webserver do
  subject(:parser) { described_class.new(source: fake_iterator) }

  let(:fake_iterator) { File.foreach(fixture_filename).lazy }

  describe '#each' do
    let(:iterator_result) { parser.each }

    shared_examples 'a valid file' do |with_rows: 500|
      it 'iterates once for every row in the file' do
        expect(iterator_result.to_a.length).to be with_rows
      end

      it 'iterates once for every row in the file which should all contain valid rows' do
        expect(iterator_result.to_a).to all be_an_instance_of(LogReporter::Row::Valid)
      end
    end

    shared_examples 'a valid file with original data' do
      it 'returns a Row::Valid on the first call to next with the correct data in' do
        expect(iterator_result.first).to be_a(LogReporter::Row::Valid)
          .and have_attributes(path: '/help_page/1', query: nil, host: nil, port: nil, scheme: nil,
                               ip_address: '126.318.035.038')
      end

      it 'returns a Row::Valid on the second call to next with the correct data in' do
        iterator_result.next
        expect(iterator_result.next).to be_a(LogReporter::Row::Valid)
          .and have_attributes path: '/contact', query: nil, host: nil,
                               port: nil, scheme: nil, ip_address: '184.123.665.067'
      end
    end

    context 'with valid file' do
      let(:fixture_filename) { './spec/fixtures/webserver.log' }

      it_behaves_like 'a valid file', with_rows: 500
      it_behaves_like 'a valid file with original data'
    end

    context 'with a file containing a mixture of valid and invalid rows' do
      let(:fixture_filename) { './spec/fixtures/webserver-some-invalid.log' }

      it 'iterates the first 6 times with error rows' do
        expect(iterator_result.first(6)).to all be_an_instance_of(LogReporter::Row::InValid)
      end

      it 'iterates over the rest with valid rows' do
        expect(iterator_result.drop(6)).to all be_an_instance_of(LogReporter::Row::Valid)
      end

      it 'populates the correct error data for the first 6 rows' do
        result = iterator_result.first(6).map(&:reason)
        expect(result).to all(eql 'Invalid row')
      end
    end

    context 'with a file containing valid data but extra fields at the end' do
      let(:fixture_filename) { './spec/fixtures/webserver-extra-fields.log' }

      it_behaves_like 'a valid file', with_rows: 500
      it_behaves_like 'a valid file with original data'
    end

    context 'with a file containing original data but with query parameters' do
      let(:fixture_filename) { './spec/fixtures/webserver-with-query-params.log' }

      it_behaves_like 'a valid file', with_rows: 500

      it 'returns a Row::Valid on the first call to next with the correct data in' do
        expect(iterator_result.first).to be_a(LogReporter::Row::Valid)
          .and have_attributes(path: '/help_page/1',
                               query: 'query=1&query2=2',
                               ip_address: '126.318.035.038',
                               host: nil, port: nil, scheme: nil)
      end
    end

    context 'with a file containing original data but with a full url and query params' do
      let(:fixture_filename) { './spec/fixtures/webserver-with-full-url-and-query-params.log' }

      it_behaves_like 'a valid file', with_rows: 500
      it 'returns a Row::Valid on the first call to next with the correct data in' do
        expect(iterator_result.first).to be_a(LogReporter::Row::Valid)
          .and have_attributes(path: '/help_page/1', query: 'query=1&query2=2',
                               host: 'some.domain.com', port: 1234, scheme: 'http',
                               ip_address: '126.318.035.038')
      end
    end
  end
end
