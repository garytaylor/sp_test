# frozen_string_literal: true

require 'log_reporter/report/text/page_hits'
require 'log_reporter/row/valid'
require 'log_reporter/row/in_valid'
RSpec.describe LogReporter::Report::Text::PageHits do
  subject(:report) { described_class.new(source: fake_iterator) }

  let(:header_rows) { 4 }
  let(:footer_rows) { 1 }

  describe '#each' do
    context 'with 2 simple rows' do
      let(:fake_iterator) do
        [
          LogReporter::Row::Valid.new(path: '/page1', ip_address: '192.168.0.1'),
          LogReporter::Row::Valid.new(path: '/page2', ip_address: '192.168.0.1')
        ].each
      end

      it 'iterates 2 rows of text' do
        expect(report.each.to_a[header_rows..(-1 - footer_rows)]).to contain_exactly a_string_matching(%r{/page1.*1}),
                                                                                     a_string_matching(%r{/page2.*1})
      end
    end

    context 'with 1 valid row and one invalid row' do
      let(:fake_iterator) do
        [
          LogReporter::Row::Valid.new(path: '/page1', ip_address: '192.168.0.1'),
          LogReporter::Row::InValid.new(reason: 'Invalid path')
        ].each
      end

      it 'iterates 1 row of text' do
        expect(report.each.to_a[header_rows..(-1 - footer_rows)]).to contain_exactly a_string_matching(%r{/page1.*1})
      end
    end

    context 'with 4 rows, the latter 3 being the same page but the last 2 are the same ip address' do
      let(:fake_iterator) do
        [
          LogReporter::Row::Valid.new(path: '/page2', ip_address: '192.168.0.1'),
          LogReporter::Row::Valid.new(path: '/page1', ip_address: '192.168.0.2'),
          LogReporter::Row::Valid.new(path: '/page1', ip_address: '192.168.0.3'),
          LogReporter::Row::Valid.new(path: '/page1', ip_address: '192.168.0.3')
        ].each
      end

      it 'yields page 1 first with 2 unique hits as it has the most visits' do
        expect(report.each.drop(header_rows).first).to match(%r{/page1.*2})
      end

      it 'yields page 2 second which has less hits' do
        expect(report.each.drop(header_rows + 1).first).to match(%r{/page2.*1})
      end
    end
  end
end
