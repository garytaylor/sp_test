# frozen_string_literal: true

require 'log_reporter/report'
RSpec.describe LogReporter::Report do
  subject(:output) { described_class }

  let(:fake_iterator) { %w[line1 line2].each }

  describe '.for' do
    context 'when report is page_hits and the format is text' do
      let(:fake_report_class) { class_spy('LogReporter::Report::Text::PageHits', new: fake_report_instance) }
      let(:fake_report_instance) { instance_spy('LogReporter::Report::Text::PageHits') }

      before { fake_report_class.as_stubbed_const }

      it 'requests a new LogReporter::Report::Text::PageHits instance' do
        described_class.for(:page_hits, format: :text, source: fake_iterator)
        expect(fake_report_class).to have_received(:new).with(source: fake_iterator)
      end

      it 'returns the new instance' do
        result = described_class.for(:page_hits, format: :text, source: fake_iterator)
        expect(result).to be fake_report_instance
      end
    end
  end
end
