# frozen_string_literal: true

require 'log_reporter/input/file'
RSpec.describe LogReporter::Input::File do
  subject(:input) { described_class.new(source: fixture_filename) }

  let(:fixture_filename) { './spec/fixtures/webserver.log' }

  describe '#each' do
    it 'returns a lazy iterator' do
      expect(input.each).to respond_to(:force)
    end

    it 'iterates the first line as a string' do
      expect(input.each.first).to eql "/help_page/1 126.318.035.038\n"
    end
  end
end
