# frozen_string_literal: true

require 'log_reporter/report_builder'
RSpec.describe LogReporter::ReportBuilder do
  subject(:builder) do
    described_class.new(parser_class: fake_parser_class,
                        report_class: fake_report_class,
                        input_class: fake_input_class,
                        output_class: fake_output_class)
  end

  let(:fake_input_class) { class_spy('LogReporter::Input', for: fake_input_instance) }
  let(:fake_input_instance) { instance_spy('LogReporter::Input::File', each: :fake_input_iterator) }
  let(:fake_parser_class) { class_spy('LogReporter::Parser', for: fake_parser_instance) }
  let(:fake_parser_instance) { instance_spy('LogReporter::Parser::Webserver', each: :fake_parser_iterator) }
  let(:fake_report_class) { class_spy('LogReporter::Report', for: fake_report_instance) }
  let(:fake_report_instance) { instance_spy('LogReporter::Report::Text::PageHits', each: :fake_report_iterator) }
  let(:fake_output_class) { class_spy('LogReporter::Output', for: fake_output_instance) }
  let(:fake_output_instance) { instance_spy('LogReporter::Output::Stdout') }

  describe '#call' do
    let(:fake_filename) { '/path/to_file' }

    before do
      builder
        .input(source: fake_filename, source_type: :file)
        .parse(format: :webserver)
        .report(:page_hits, format: :text)
        .output(dest_type: :stdout)
        .call
    end

    it 'requests an input instance correctly' do
      expect(fake_input_class).to have_received(:for)
        .with(source: fake_filename, source_type: :file)
    end

    it 'stores the input instance' do
      expect(builder.input_instance).to be fake_input_instance
    end

    it 'requests a parser instance correctly' do
      expect(fake_parser_class).to have_received(:for)
        .with(format: :webserver, source: :fake_input_iterator)
    end

    it 'stores the parser instance' do
      expect(builder.parser_instance).to be fake_parser_instance
    end

    it 'requests a report correctly' do
      expect(fake_report_class).to have_received(:for)
        .with(:page_hits, format: :text, source: :fake_parser_iterator)
    end

    it 'stores the requested report' do
      expect(builder.report_instance).to be fake_report_instance
    end

    it 'requests an output instance correctly' do
      expect(fake_output_class).to have_received(:for)
        .with(dest_type: :stdout, source: :fake_report_iterator)
    end

    it 'stores the requested output instance' do
      expect(builder.output_instance).to be fake_output_instance
    end

    it 'calls call on the final output object' do
      expect(fake_output_instance).to have_received(:call)
    end
  end
end
