# frozen_string_literal: true

require 'log_reporter/output'
RSpec.describe LogReporter::Output do
  subject(:output) { described_class }

  let(:fake_iterator) { %w[line1 line2].each }

  describe '.for' do
    context 'when dest_type is :stdout' do
      let(:fake_stdout_class) { class_spy('LogReporter::Output::Stdout', new: fake_stdout_instance) }
      let(:fake_stdout_instance) { instance_spy('LogReporter::Output::Stdout') }

      before { fake_stdout_class.as_stubbed_const }

      it 'requests a new Output::StdOut instance' do
        described_class.for(source: fake_iterator, dest_type: :stdout)
        expect(fake_stdout_class).to have_received(:new).with(source: fake_iterator)
      end

      it 'returns the new instance' do
        result = described_class.for(source: fake_iterator, dest_type: :stdout)
        expect(result).to be fake_stdout_instance
      end
    end
  end
end
