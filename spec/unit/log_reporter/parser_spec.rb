# frozen_string_literal: true

require 'log_reporter/parser'
RSpec.describe LogReporter::Parser do
  subject(:parser) { described_class }

  let(:fake_iterator) { %w[line1 line2].each }

  describe '.for' do
    context 'when format is :webserver' do
      let(:fake_parser_class) { class_spy('LogReporter::Parser::Webserver', new: fake_parser_instance) }
      let(:fake_parser_instance) { instance_spy('LogReporter::Parser::Webserver') }

      before { fake_parser_class.as_stubbed_const }

      it 'requests a new Parser::Webserver instance' do
        parser.for(source: fake_iterator, format: :webserver)
        expect(fake_parser_class).to have_received(:new).with(source: fake_iterator)
      end

      it 'returns the new instance' do
        result = parser.for(source: fake_iterator, format: :webserver)
        expect(result).to be fake_parser_instance
      end
    end
  end
end
