# frozen_string_literal: true

require 'log_reporter/input'
RSpec.describe LogReporter::Input do
  subject(:output) { described_class }

  describe '.for' do
    context 'when source_type is :file' do
      let(:fake_input_class) { class_spy('LogReporter::Input::File', new: fake_input_instance) }
      let(:fake_input_instance) { instance_spy('LogReporter::Input::File') }
      let(:fake_filename) { '/path/to/fake/file' }

      before { fake_input_class.as_stubbed_const }

      it 'requests a new Input::File instance' do
        described_class.for(source: fake_filename, source_type: :file)
        expect(fake_input_class).to have_received(:new).with(source: fake_filename)
      end

      it 'returns the new instance' do
        result = described_class.for(source: fake_filename, source_type: :file)
        expect(result).to be fake_input_instance
      end
    end
  end
end
