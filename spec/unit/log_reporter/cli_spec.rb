# frozen_string_literal: true

require 'log_reporter/cli'
require 'log_reporter/parser'
require 'log_reporter/reporter'
RSpec.describe LogReporter::Cli do
  subject(:cli) { described_class }

  describe '.start' do
    let(:fake_report_builder) { instance_spy('LogReporter::ReportBuilder') }

    context 'when using the default command with an example file' do
      let(:fixture_file) { './spec/fixtures/webserver.log' }
      let(:report_type) { :page_hits }
      let(:args) { [fixture_file] }

      before { cli.start(args, report_builder: fake_report_builder) }

      it 'calls the input method on the builder with the correct args' do
        expect(fake_report_builder).to have_received(:input).with(source: fixture_file,
                                                                  source_type: :file)
      end

      it 'calls the parse method on the builder with the correct args' do
        expect(fake_report_builder).to have_received(:parse).with(format: :webserver)
      end

      it 'calls the report method on the builder with the correct args' do
        expect(fake_report_builder).to have_received(:report).with(:page_hits, format: :text)
      end

      it 'calls the output method on the builder with the correct args' do
        expect(fake_report_builder).to have_received(:output).with(dest_type: :stdout)
      end

      it 'calls the call method on the builder' do
        expect(fake_report_builder).to have_received(:call).with(no_args)
      end
    end

    context 'when the file does not exist' do
      let(:fixture_file) { './spec/fixtures/wrongfile.log' }
      let(:report_type) { :page_hits }
      let(:args) { [fixture_file] }

      it 'outputs an error message to stderr' do
        expect { cli.start(args, report_builder: fake_report_builder) }
          .to output("File #{File.expand_path(fixture_file, Dir.pwd)} not found\n").to_stderr
      end
    end
  end
end
