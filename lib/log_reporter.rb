# frozen_string_literal: true

require 'log_reporter/version'

module LogReporter
  class Error < StandardError; end
  # Your code goes here...
end
