# frozen_string_literal: true

module LogReporter
  module Exceptions
    class Base < StandardError
    end

    class InvalidDestination < Base
    end
  end
end
