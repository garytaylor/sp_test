# frozen_string_literal: true

module LogReporter
  # Utilities for strings to save using something big like active support.
  module StringUtils
    class << self
      # Camelize the string - single level only, no nested support yet
      # @param [String] string The string to camelize
      # @return [String] The camelized string
      def camelize(string)
        string.split('_').map(&:capitalize).join('')
      end
    end
  end
end
