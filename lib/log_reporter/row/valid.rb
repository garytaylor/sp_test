# frozen_string_literal: true

module LogReporter
  module Row
    Valid = Struct.new(:scheme, :host, :port, :path, :query, :ip_address, keyword_init: true)
  end
end
