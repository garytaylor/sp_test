# frozen_string_literal: true

module LogReporter
  module Row
    InValid = Struct.new(:reason, keyword_init: true)
  end
end
