# frozen_string_literal: true

require 'log_reporter/row/valid'
require 'log_reporter/row/in_valid'
require 'uri'
module LogReporter
  class Parser
    # Parses the input stream in the 'webserver' format producing an
    # iterator over normalized row objects
    class Webserver
      def initialize(source:)
        @source = source
      end

      def each
        enum_for(:yield_rows)
      end

      private

      attr_reader :source

      def yield_rows
        source.each do |row|
          yield parse_row(row)
        end
      end

      def parse_row(row)
        path, ip_address = row.split(' ').map(&:strip)
        uri = URI.parse(path)
        if valid_path?(uri.path) && valid_ip_address?(ip_address)
          LogReporter::Row::Valid.new path: uri.path, query: uri.query, host: uri.host,
                                      port: uri.port, scheme: uri.scheme, ip_address: ip_address
        else
          LogReporter::Row::InValid.new(reason: 'Invalid row')
        end
      rescue URI::InvalidURIError
        LogReporter::Row::InValid.new(reason: 'Invalid row')
      end

      def valid_path?(path)
        path.start_with?('/')
      end

      def valid_ip_address?(ip_address)
        return false if ip_address.nil? || ip_address.empty?

        ip_address =~ /\A(\d|\.)+\z/
      end
    end
  end
end
