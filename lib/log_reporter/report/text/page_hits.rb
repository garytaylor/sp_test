# frozen_string_literal: true

module LogReporter
  class Report
    module Text
      # Reports on page hits summarised using a count of number of hits per page
      class PageHits
        def initialize(source:)
          @source = source
        end

        def each
          enum_for(:yield_rows)
        end

        private

        attr_reader :source

        def yield_rows
          header_rows.each { |header_row| yield header_row }
          sorted_report_data.each do |report_row|
            yield present_row(report_row[:path], report_row[:count]) unless report_row[:path] == :errors
          end
          yield footer_row
        end

        def sorted_report_data
          report_data.sort do |a, b|
            b[:count] <=> a[:count]
          end
        end

        def report_data
          grouped_source.map do |(path, rows)|
            if path == :errors
              { path: path, count: rows.length }
            else
              { path: path, count: rows.map(&:ip_address).uniq.length }
            end
          end
        end

        def grouped_source
          source.group_by do |row|
            row.is_a?(LogReporter::Row::Valid) ? row.path : :errors
          end
        end

        def present_row(path, count)
          "| #{path.ljust(65, ' ')} | #{count.to_s.center(8)} |"
        end

        def header_rows
          [
            '|-------------------------------------------------------------------|----------|',
            '| Path                                                              |  Unique  |',
            '|                                                                   |  views   |',
            '|-------------------------------------------------------------------|----------|'
          ]
        end

        def footer_row
          '|-------------------------------------------------------------------|----------|'
        end
      end
    end
  end
end
