# frozen_string_literal: true

require 'log_reporter/input'
require 'log_reporter/output'
require 'log_reporter/parser'
require 'log_reporter/report'
module LogReporter
  # Provides a single object to coordinate the work of producing a report
  # from the arguments given.
  class ReportBuilder
    def initialize(parser_class: Parser, report_class: Report, input_class: Input, output_class: Output)
      @parser_class = parser_class
      @report_class = report_class
      @input_class = input_class
      @output_class = output_class
      @args = {}
    end

    # @TODO These instances should really be private - the only reason to make them public is for testing.
    #  Need to think of a way to test the outcome instead.
    attr_reader :input_instance, :parser_instance, :report_instance, :output_instance

    def input(source:, source_type:)
      args[:input] = { source: source, source_type: source_type }
      self
    end

    def parse(format:)
      args[:parser] = { format: format }
      self
    end

    def report(type, format:)
      args[:report] = { type: type, format: format }
      self
    end

    def output(dest_type:)
      args[:output] = { dest_type: dest_type }
      self
    end

    def call
      build
      output_instance.call
    end

    private

    def build
      build_input
      build_parser
      build_report
      build_output
    end

    def build_output
      return unless args.key?(:output)

      @output_instance = output_class.for(source: report_instance.each, **args[:output])
    end

    def build_report
      return unless args.key?(:report)

      @report_instance = report_class.for(args[:report][:type],
                                          source: parser_instance.each,
                                          format: args[:report][:format])
    end

    def build_parser
      return unless args.key?(:parser)

      @parser_instance = parser_class.for(source: input_instance.each, **args[:parser])
    end

    def build_input
      return unless args.key?(:input)

      @input_instance = input_class.for(**args[:input])
    end

    attr_reader :parser_class, :report_class, :input_class, :output_class, :args
  end
end
