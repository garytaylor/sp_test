# frozen_string_literal: true

module LogReporter
  class Input
    # Provides a lazy iterator to read each line of the input file
    class File
      def initialize(source:)
        @source = source
      end

      def each
        ::File.foreach(source).lazy
      end

      private

      attr_reader :source
    end
  end
end
