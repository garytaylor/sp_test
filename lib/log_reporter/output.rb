# frozen_string_literal: true

require 'log_reporter/exceptions'
require 'log_reporter/string_utils'
Dir.glob(File.join(__dir__, 'output', '**', '*.rb')).sort.each { |filename| require filename }

module LogReporter
  # A factory object for providing instances of the correct type of output based on
  # the input arguments.
  class Output
    # Generate an output adapter instance
    # @param [Symbol] dest_type The type of output (e.g. ::stdout)
    # @param [Enumerator] source The source data in the form of an enumerator
    # @return [LogReporter::Output::Stdout] The output adapter instance
    def self.for(source:, dest_type:)
      adapter_class(dest_type).new(source: source)
    end

    def self.adapter_class(dest_type)
      camelized = StringUtils.camelize(dest_type.to_s)
      raise Exceptions::InvalidDestination, "Invalid destination #{dest_type}" unless const_defined?(camelized)

      const_get(camelized)
    end

    private_class_method :adapter_class
  end
end
