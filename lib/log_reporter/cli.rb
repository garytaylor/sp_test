# frozen_string_literal: true

require 'log_reporter/report_builder'
module LogReporter
  # This class is a form of controller for the command line interface.
  # It doesn't actually do the work, but delegates it to the appropriate place
  class Cli
    # Starts the command line parsing - executing the only command possible for now.
    # @param [Array<String>] args An array of arguments as passed from the ARGV array
    def self.start(cli_args, *args)
      new(cli_args).start(*args)
    end

    # Dispatches to the appropriate place depending on the command given (only 1 at the moment)
    def start(*args)
      catch(:error) do
        validate_report_args
        generate_report(*args)
      end
    end

    private

    attr_reader :args

    def validate_report_args
      filename = args.first
      full_file_path = File.expand_path(filename, Dir.pwd)
      return if File.exist?(full_file_path)

      warn("File #{full_file_path} not found")
      throw(:error)
    end

    def generate_report(report_builder: ReportBuilder.new)
      filename = args.first
      report_builder
        .input(source: filename, source_type: :file)
        .parse(format: :webserver)
        .report(:page_hits, format: :text)
        .output(dest_type: :stdout)
        .call
    end

    # @private - the caller should always go via the start method on the class
    def initialize(args)
      @args = args
    end
  end
end
