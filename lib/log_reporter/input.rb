# frozen_string_literal: true

require 'log_reporter/string_utils'
Dir.glob(File.join(__dir__, 'input', '**', '*.rb')).sort.each { |filename| require filename }
module LogReporter
  # A factory object for providing instances of the correct type of input based on
  # the input arguments.
  class Input
    # Generate an input adapter instance
    # @param [Symbol] source_type The type of input (e.g. ::file)
    # @param [String] source The source data (e.g. filename for a :file)
    # @return [LogReporter::Input::File] The input adapter instance
    def self.for(source:, source_type:)
      adapter_class(source_type).new(source: source)
    end

    def self.adapter_class(source_type)
      camelized = StringUtils.camelize(source_type.to_s)
      raise Exceptions::InvalidDestination, "Invalid source type #{source_type}" unless const_defined?(camelized)

      const_get(camelized)
    end

    private_class_method :adapter_class
  end
end
