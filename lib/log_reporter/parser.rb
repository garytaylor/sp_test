# frozen_string_literal: true

require 'log_reporter/string_utils'
Dir.glob(File.join(__dir__, 'parser', '**', '*.rb')).sort.each { |filename| require filename }
module LogReporter
  # A factory object for providing instances of the correct type of parser based on
  # the input arguments.
  class Parser
    # Generate a parser instance
    # @param [Symbol] format The format (e.g :webserver)
    # @param [Enumerator] source The source data in the form of an enumerator
    # @return [LogReporter::Parser::Webserver] The report instance
    def self.for(source:, format:)
      adapter_class(format).new(source: source)
    end

    def self.adapter_class(format)
      camelized = StringUtils.camelize(format.to_s)
      raise Exceptions::InvalidDestination, "Invalid parser format #{format}" unless const_defined?(camelized)

      const_get(camelized)
    end

    private_class_method :adapter_class
  end
end
