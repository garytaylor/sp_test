# frozen_string_literal: true

module LogReporter
  class Output
    # An output adapter to write the stream to STDOUT
    class Stdout
      def initialize(source:)
        @source = source
      end

      def call
        source.each do |str|
          puts str
        end
      end

      private

      attr_reader :source
    end
  end
end
