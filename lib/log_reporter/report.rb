# frozen_string_literal: true

require 'log_reporter/string_utils'
Dir.glob(File.join(__dir__, 'report', '**', '*.rb')).sort.each { |filename| require filename }

module LogReporter
  # A factory object for providing instances of the correct type of report based on
  # the input arguments.
  class Report
    # Generate a report instance
    # @param [Symbol] report The type of report (e.g. :page_hits)
    # @param [Symbol] format The format (e.g :text)
    # @param [Enumerator] source The source data in the form of an enumerator
    # @return [LogReporter::Report::Text::PageHits] The report instance
    def self.for(report, format:, source:)
      adapter_class(report, format).new(source: source)
    end

    def self.adapter_class(report, format)
      camelized_report = StringUtils.camelize(report.to_s)
      camelized_format = StringUtils.camelize(format.to_s)
      unless const_defined?(camelized_format) && const_get(camelized_format).const_defined?(camelized_report)
        raise Exceptions::InvalidDestination, "Invalid report #{report} with format #{format}"
      end

      const_get(camelized_format).const_get(camelized_report)
    end

    private_class_method :adapter_class
  end
end
