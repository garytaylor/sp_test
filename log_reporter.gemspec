# frozen_string_literal: true

require_relative 'lib/log_reporter/version'

Gem::Specification.new do |spec|
  spec.name          = 'log_reporter'
  spec.version       = LogReporter::VERSION
  spec.authors       = ['Gary Taylor']
  spec.email         = ['gary.taylor@hismessages.com']

  spec.summary       = 'Parses a log file and produces a report'
  spec.description   = 'Given a large log file, produces a report showing the hit count of pages'
  spec.homepage      = 'https://gitlab.com/garytaylor/sp_test'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/garytaylor/sp_test'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
end
